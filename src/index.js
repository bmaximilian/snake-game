/**
 * Game of snake for one player
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
import Game from './lib/Game';

const game = new Game();
const start = () => {
    "use strict";
    let name = document.getElementById("nickname").value;

    if (name.length > 0) {
        game.start(name);
        document.getElementById("form-container").classList.add("hidden");
    }
};

document.getElementById("start").addEventListener("click", start());
document.getElementById("nickname").addEventListener("keypress", (e) => {
    let keyCode = e.keyCode ? e.keyCode : window.event.keyCode;
    if (keyCode == 13)
        start();
});