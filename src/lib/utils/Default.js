/**
 * Helper class
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
class DefaultUtils {

    

    /**
     * Returns a random integer in the given range
     *
     * @param min
     * @param max
     * @returns {*}
     */
    static getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min;
    }

    /**
     * Checks if the given array contains a specific value
     *
     * @param haystack
     * @param needle
     * @returns {boolean}
     */
    static arrayContains(haystack, needle) {
        if (Array.isArray(haystack))
            for (let element of haystack)
                if (element === needle)
                    return true;
        return false;
    }
}

export default DefaultUtils;