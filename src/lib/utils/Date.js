/**
 * Helper class for events
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
class DateUtils {
    /**
     * Converts a Date object to SQL timestamp
     *
     * @param date Date
     * @returns {string}
     */
    static dateToTimestamp(date) {

        let month = date.getMonth() + 1;
        if (month < 10)
            month = "0" + month;

        let day = date.getDate();
        if (day < 10)
            day = "0" + day;

        let hour = date.getHours();
        if (hour < 10)
            hour = "0" + hour;

        let minute = date.getMinutes();
        if (minute < 10)
            minute = "0" + minute;

        let second = date.getSeconds();
        if (second < 10)
            second = "0" + second;

        return date.getFullYear() + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
    }

    /**
     * Returns the date object in milliseconds
     *
     * @param dateString
     * @returns {number}
     */
    static getMilliseconds(dateString) {
        let date = dateString;

        if (!(dateString instanceof Date))
            date = new Date(dateString);

        let totalMs = 0;
        totalMs += (date.getMonth() + 1) * 2630000000;
        totalMs += date.getDate() * 86400000;
        totalMs += date.getHours() * 3600000;
        totalMs += date.getMinutes() * 60000;
        totalMs += date.getSeconds() * 1000;

        return totalMs;
    }

    /**
     * Returns difference in milliseconds
     *
     * @param startdate
     * @param enddate
     * @returns {number}
     */
    static getDifferenceInMilliseconds(startdate, enddate = null) {
        return Math.abs(DateUtils.getMilliseconds(startdate) - DateUtils.getMilliseconds(enddate));
    }
}

export default DateUtils;