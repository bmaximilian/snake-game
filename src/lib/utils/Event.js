/**
 * Helper class for events
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
class EventUtils {

    /**
     * Constructor of Event
     *
     * @param name
     * @param element
     */
    constructor(name, element) {
        EventUtils.fire(name, element);
    }

    /**
     * Emits an event on the submitted element with the submitted name
     *
     * @param name
     * @param element
     */
    static fire(name, element) {
        let event;

        if (document.createEvent) {
            event = document.createEvent("HTMLEvents");
            event.initEvent(name, true, true);
        } else {
            event = document.createEventObject();
            event.eventType = name;
        }

        event.eventName = name;

        if (document.createEvent) {
            element.dispatchEvent(event);
        } else {
            element.fireEvent("on" + event.eventType, event);
        }
    }
}

export default EventUtils;