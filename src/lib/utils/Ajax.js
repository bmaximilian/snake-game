/**
 * Class for sending Ajax requests
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
class AjaxUtils {

    /**
     * Constructor of class AjaxRequest
     *
     * @author Maximilian Beck <contact@maximilianbeck.de>
     *
     * @param options
     * @param callback
     * @param errorCallback
     */
    constructor(options = {method: "GET", data: null, url: null}, callback, errorCallback = null) {
        this._method = (options.method ? options.method : "GET");
        this._data = options.data;
        this._url = options.url;
        this._callback = callback;
        this._errorCallback = errorCallback;

        this._formData = new FormData();
        this._xhr = new XMLHttpRequest();

        this._setData();
        this._createXmlHttpRequest();

        this._xhr.send(this._formData);
    }

    /**
     * Function to create the XMLHttpRequest
     *
     * @author Maximilian Beck <contact@maximilianbeck.de>
     *
     * @private
     */
    _createXmlHttpRequest() {
        const self = this;
        self._xhr.open(self._method, self._url, true);

        if (self._method.toLowerCase() === "post")
            self._xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        self._xhr.onreadystatechange = function () {

            try {

                if (this.readyState === XMLHttpRequest.DONE && this.status === 200 && self._callback)
                    self._callback(this.responseText);

                else if (this.readyState === XMLHttpRequest.DONE && this.status >= 400 && self._errorCallback)
                    self._errorCallback(this.responseText, this.status, this.statusText);

            } catch (e) {
                console.error(e);
            }
        }
    }

    /**
     * Function to append data to the request
     *
     * @author Maximilian Beck <contact@maximilianbeck.de>
     *
     * @private
     */
    _setData() {
        if (this._data)
            for (let key in this._data)
                if (this._data.hasOwnProperty(key))
                    this._formData.append(key, this._data[key]);
    }

}

export default AjaxUtils;