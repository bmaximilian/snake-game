import DateUtils from './utils/Date';
import DefaultUtils from './utils/Default';
import Playground from './Playground';
import Snake from './Snake';
import Food from './Food';

/**
 * Main class of snake game
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
class Game extends Playground {

    /**
     * Constructor of snake game
     * Loads game. Needs to be started
     */
    constructor() {
        super();
        this.SPEED = 100;
        this.NICKNAME = "Player 1";

        this.SNAKE_SIZE = 10;
        this.FOOD_SIZE = 10;
        this.DIRECTION_UP = 0;
        this.DIRECTION_DOWN = 1;
        this.DIRECTION_LEFT = 2;
        this.DIRECTION_RIGHT = 3;
        this.started = false;

        this.direction = this.oldDirection = this.DIRECTION_LEFT;
        this.begin = this.end = this.gameloop = null;
        this.moves = 0;

        this.snake = new Snake(5, this.SNAKE_SIZE);
        this.foods = [new Food(this.snake.elements, Food.TYPE_FOOD, this.FOOD_SIZE)];
    }

    /**
     * Stops game
     */
    stop() {
        console.log('game finished');
        this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
        this.gameloop = cancelAnimationFrame(this.gameloop);
        this.started = false;
        this.end = new Date();
        console.log({
            name: this.NICKNAME,
            score: this.snake.score,
            moves: this.moves,
            duration: DateUtils.getDifferenceInMilliseconds(this.begin, this.end),
            begin: DateUtils.dateToTimestamp(this.begin),
            end: DateUtils.dateToTimestamp(this.end)
        });
    }

    /**
     * Checks if food is in range of the head
     */
    lookForFood() {
        for (let i = 0; i < this.foods.length; i++)
            if (Math.round(this.snake.elements[0].x - this.foods[i].x) === 0 && Math.round(this.snake.elements[0].y - this.foods[i].y) === 0) {

                switch (this.foods[i].type) {
                    case Food.TYPE_FOOD:
                        this.snake.eat();
                        break;
                    case Food.TYPE_STONE:
                        this.stop();
                        break;
                    default:
                        this.snake.eat();
                        break;
                }

                if (this.snake.score > 30)
                    for (let k = 0; k < this.foods.length; k++)
                        this.foods[k] = new Food(this.snake.elements, this.foods[k].type, this.FOOD_SIZE);
                else
                    this.foods[i] = new Food(this.snake.elements, this.foods[i].type, this.FOOD_SIZE);


                if (this.snake.score % 5 === 0)
                    this.foods = [...this.foods, new Food(this.snake.elements, Food.TYPE_FOOD, this.FOOD_SIZE)];

                if (this.snake.score % 3 === 0)
                    this.foods = [...this.foods, new Food(this.snake.elements, Food.TYPE_STONE, this.FOOD_SIZE)];

                for (let k = 0; k < this.foods.length; k++)
                    for (let x = 0; x < this.foods.length; x++)
                        if (this.foods[k].x === this.foods[x].x && this.foods[k].y === this.foods[x].y && x !== k)
                            this.foods[k][DefaultUtils.getRandomInt(1, 10) % 2 === 0 ? "x" : "y"]++;

            }
    }

    /**
     * Returns function to listen on keys required for the game
     *
     * @param game
     * @returns {function()}
     * @private
     */
    _keyListener(game) {
        return (event) => {
            let keyCode = event.keyCode ? event.keyCode : window.event.keyCode;

            switch (keyCode) {

                case 37:
                    if (game.direction != game.DIRECTION_RIGHT && game.direction === game.oldDirection) {
                        game.direction = game.DIRECTION_LEFT;
                        game.moves++;
                    }
                    break;

                case 39:
                    if (game.direction != game.DIRECTION_LEFT  && game.direction === game.oldDirection) {
                        game.direction = game.DIRECTION_RIGHT;
                        game.moves++;
                    }
                    break;

                case 38:
                    if (game.direction != game.DIRECTION_DOWN  && game.direction === game.oldDirection) {
                        game.direction = game.DIRECTION_UP;
                        game.moves++;
                    }
                    break;

                case 40:
                    if (game.direction != game.DIRECTION_UP  && game.direction === game.oldDirection) {
                        game.direction = game.DIRECTION_DOWN;
                        game.moves++;
                    }
                    break;
            }
        };
    }

    /**
     * Main function of the game
     * Animates the snake
     *
     * @param game
     * @returns {function()}
     */
    main(game) {
        return() => {
            game.snake.elements[0].x = Math.round(game.snake.elements[0].x * 10) / 10;
            game.snake.elements[0].y = Math.round(game.snake.elements[0].y * 10) / 10;

            if (game.snake.elements[0].x % 1 === 0 && game.snake.elements[0].y % 1 === 0)
                game.oldDirection = game.direction;

            switch (game.oldDirection) {
                case game.DIRECTION_RIGHT:
                    game.snake.elements[0].x += 0.01 * this.SPEED;
                    break;
                case game.DIRECTION_LEFT:
                    game.snake.elements[0].x -= 0.01 * this.SPEED;
                    break;
                case game.DIRECTION_UP:
                    game.snake.elements[0].y -= 0.01 * this.SPEED;
                    break;
                case game.DIRECTION_DOWN:
                    game.snake.elements[0].y += 0.01 * this.SPEED;
                    break;
            }

            if (game.snake.check())
                game.stop();

            game.lookForFood();
            game.snake.move();

            game.ctx.clearRect(0, 0, game.ctx.canvas.width, game.ctx.canvas.height);
            game.snake.draw();
            game.snake.setScoreText();

            for (let food of game.foods)
                food.drawFood();

            if (game.started) {
                setTimeout(() => game.gameloop = requestAnimationFrame(game.main(game)), 70);
            }
        }
    }

    /**
     * Function to start the game
     */
    start(name = "Player 1") {
        window.addEventListener("keydown", this._keyListener(this));
        this.started = true;
        this.begin = new Date();
        this.NICKNAME = name;
        this.gameloop = requestAnimationFrame(this.main(this));
    }
}

export default Game;