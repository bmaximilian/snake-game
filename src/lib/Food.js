import Utils from './utils/Default';
import Playground from './Playground';

/**
 * Class for a food element
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
class Food extends Playground {

    /**
     * Constructor of food element
     *
     * @param snakeElements
     * @param type
     * @param size
     */
    constructor(snakeElements, type = 0, size = 10) {
        super();
        this.size = size;

        this.x = Utils.getRandomInt(1, Math.round(this.ctx.canvas.width / this.size) - 1);
        this.y = Utils.getRandomInt(1, Math.round(this.ctx.canvas.height / this.size) - 1);
        this.type = type;

        for (let element of snakeElements) {
            if (this.x === element.x)
                this.x++;
            if (this.y === element.y)
                this.y++;
        }

        this.drawFood();
    }

    /**
     * Draws food element on canvas context
     */
    drawFood() {
        switch (this.type) {
            case 0:
                this.ctx.fillStyle = "red";
                this.ctx.fillRect(this.x * this.size, this.y * this.size, this.size, this.size);
                break;
            case 1:
                this.ctx.fillStyle = "black";
                this.ctx.fillRect(this.x * this.size, this.y * this.size, this.size, this.size);
                break;
        }
    }
}

Food.TYPE_FOOD = 0;
Food.TYPE_STONE = 1;

export default Food;
