/**
 * Basic Element of the Game
 * contains canvas and context
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
class Playground {

    /**
     * Playground constructor
     */
    constructor() {
        this.canvas = document.getElementById('game');
        this.ctx = this.canvas.getContext('2d');
        this.ctx.canvas.width  = window.innerWidth;
        this.ctx.canvas.height = window.innerHeight;
        this.w = window.innerWidth;
        this.h =  window.innerHeight;

        window.addEventListener("resize", this._windowResize(this));
    }

    /**
     * Returns function to change the width and height properties
     * of the playground
     *
     * @param game
     * @returns {function()}
     * @private
     */
    _windowResize(game) {
        return () => {
            game.ctx.canvas.width  = window.innerWidth;
            game.ctx.canvas.height = window.innerHeight;
            game.w = window.innerWidth;
            game.h = window.innerHeight;
        };
    }

}

export default Playground;