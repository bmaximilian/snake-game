import Utils from './utils/Default';
import Playground from './Playground';

/**
 * Class of the game character (the snake)
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
class Snake extends Playground {

    /**
     * Creates the snake at the center of the playground
     *
     * @param length
     * @param size
     */
    constructor(length = 4, size = 10) {
        super();

        let X_START = Math.round((this.w / 10) / 2);
        let Y_START = Math.round((this.h / 10) / 2);

        this.size = size;
        this.elements = [];
        this.score = 0;


        for (let i = X_START; i <= X_START+length; i++)
            if (!Utils.arrayContains(this.elements, {x: i, y: Y_START}))
                this.elements.push({x: i, y: Y_START});


        this.setScoreText();
    }

    /**
     * Function to draw the body of the snake
     *
     * @param x
     * @param y
     * @private
     */
    _setBody(x, y) {
        this.ctx.fillStyle = "green";
        this.ctx.fillRect(x * this.size, y * this.size, this.size, this.size);
    }

    /**
     * Function to check if the snake bites itself
     *
     * @param x
     * @param y
     * @returns {boolean}
     * @private
     */
    _checkCollision(x, y) {
        for (let i = 1; i < this.elements.length; i++)
            if(this.elements[i].x === x && this.elements[i].y === y)
                return true;

        return false;
    }

    /**
     * Function to check if the snake collides with a border of the playground
     *
     * @param element
     * @returns {boolean}
     * @private
     */
    _checkBorder(element) {
        return (element.x <= -1
        || element.x >= Math.round(this.ctx.canvas.width / this.size) - 1
        || element.y <= -1
        || element.y >= Math.round(this.ctx.canvas.height / this.size) - 1);
    }

    /**
     * Draws the snake body on the canvas context
     */
    draw() {
        for (let element of this.elements)
            this._setBody(element.x, element.y);
    }

    /**
     * Adds a new element to the snake
     */
    eat() {
        this.score++;
        this.setScoreText();
        this.elements = [
            ...this.elements,
            {
                x: this.elements[this.elements.length -1].x,
                y: this.elements[this.elements.length -1].y
            }
        ];
    }

    /**
     * Moves the snake
     */
    move() {
        let tail = this.elements.pop();
        if (this.elements.length > 0) {
            tail.x = this.elements[0].x;
            tail.y = this.elements[0].y;
        }

        this.elements.unshift(tail);
    }

    /**
     * Function to write the score on the canvas context
     */
    setScoreText() {
        let text = "Score: " + this.score;
        this.ctx.fillStyle = 'blue';
        this.ctx.font="20px Arial";
        this.ctx.fillText(text, this.w/2, this.h - 5);
    }

    /**
     * Checks if the game is lost
     *
     * @returns {boolean}
     */
    check() {
        return this._checkBorder(this.elements[0]) || this._checkCollision(this.elements[0].x, this.elements[0].y)
    }
}

export default Snake;
