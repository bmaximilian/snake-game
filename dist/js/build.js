/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmory imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmory exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		Object.defineProperty(exports, name, {
/******/ 			configurable: false,
/******/ 			enumerable: true,
/******/ 			get: getter
/******/ 		});
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/js/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 7);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports) {

"use strict";
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Basic Element of the Game
 * contains canvas and context
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
var Playground = function () {

    /**
     * Playground constructor
     */
    function Playground() {
        _classCallCheck(this, Playground);

        this.canvas = document.getElementById('game');
        this.ctx = this.canvas.getContext('2d');
        this.ctx.canvas.width = window.innerWidth;
        this.ctx.canvas.height = window.innerHeight;
        this.w = window.innerWidth;
        this.h = window.innerHeight;

        window.addEventListener("resize", this._windowResize(this));
    }

    /**
     * Returns function to change the width and height properties
     * of the playground
     *
     * @param game
     * @returns {function()}
     * @private
     */


    _createClass(Playground, [{
        key: '_windowResize',
        value: function _windowResize(game) {
            return function () {
                game.ctx.canvas.width = window.innerWidth;
                game.ctx.canvas.height = window.innerHeight;
                game.w = window.innerWidth;
                game.h = window.innerHeight;
            };
        }
    }]);

    return Playground;
}();

exports.default = Playground;

/***/ },
/* 1 */
/***/ function(module, exports) {

"use strict";
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Helper class
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
var DefaultUtils = function () {
    function DefaultUtils() {
        _classCallCheck(this, DefaultUtils);
    }

    _createClass(DefaultUtils, null, [{
        key: "getRandomInt",


        /**
         * Returns a random integer in the given range
         *
         * @param min
         * @param max
         * @returns {*}
         */
        value: function getRandomInt(min, max) {
            min = Math.ceil(min);
            max = Math.floor(max);
            return Math.floor(Math.random() * (max - min)) + min;
        }

        /**
         * Checks if the given array contains a specific value
         *
         * @param haystack
         * @param needle
         * @returns {boolean}
         */

    }, {
        key: "arrayContains",
        value: function arrayContains(haystack, needle) {
            if (Array.isArray(haystack)) {
                var _iteratorNormalCompletion = true;
                var _didIteratorError = false;
                var _iteratorError = undefined;

                try {
                    for (var _iterator = haystack[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                        var _element = _step.value;

                        if (_element === needle) return true;
                    }
                } catch (err) {
                    _didIteratorError = true;
                    _iteratorError = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion && _iterator.return) {
                            _iterator.return();
                        }
                    } finally {
                        if (_didIteratorError) {
                            throw _iteratorError;
                        }
                    }
                }
            }return false;
        }
    }]);

    return DefaultUtils;
}();

exports.default = DefaultUtils;

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
"use strict";

var _Game = __webpack_require__(4);

var _Game2 = _interopRequireDefault(_Game);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var game = new _Game2.default(); /**
                                  * Game of snake for one player
                                  *
                                  * @author Maximilian Beck <contact@maximilianbeck.de>
                                  */

var start = function start() {
    "use strict";

    var name = document.getElementById("nickname").value;

    if (name.length > 0) {
        game.start(name);
        document.getElementById("form-container").classList.add("hidden");
    }
};

document.getElementById("start").addEventListener("click", start());
document.getElementById("nickname").addEventListener("keypress", function (e) {
    var keyCode = e.keyCode ? e.keyCode : window.event.keyCode;
    if (keyCode == 13) start();
});

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Default = __webpack_require__(1);

var _Default2 = _interopRequireDefault(_Default);

var _Playground2 = __webpack_require__(0);

var _Playground3 = _interopRequireDefault(_Playground2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Class for a food element
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
var Food = function (_Playground) {
    _inherits(Food, _Playground);

    /**
     * Constructor of food element
     *
     * @param snakeElements
     * @param type
     * @param size
     */
    function Food(snakeElements) {
        var type = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
        var size = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 10;

        _classCallCheck(this, Food);

        var _this = _possibleConstructorReturn(this, (Food.__proto__ || Object.getPrototypeOf(Food)).call(this));

        _this.size = size;

        _this.x = _Default2.default.getRandomInt(1, Math.round(_this.ctx.canvas.width / _this.size) - 1);
        _this.y = _Default2.default.getRandomInt(1, Math.round(_this.ctx.canvas.height / _this.size) - 1);
        _this.type = type;

        var _iteratorNormalCompletion = true;
        var _didIteratorError = false;
        var _iteratorError = undefined;

        try {
            for (var _iterator = snakeElements[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                var element = _step.value;

                if (_this.x === element.x) _this.x++;
                if (_this.y === element.y) _this.y++;
            }
        } catch (err) {
            _didIteratorError = true;
            _iteratorError = err;
        } finally {
            try {
                if (!_iteratorNormalCompletion && _iterator.return) {
                    _iterator.return();
                }
            } finally {
                if (_didIteratorError) {
                    throw _iteratorError;
                }
            }
        }

        _this.drawFood();
        return _this;
    }

    /**
     * Draws food element on canvas context
     */


    _createClass(Food, [{
        key: 'drawFood',
        value: function drawFood() {
            switch (this.type) {
                case 0:
                    this.ctx.fillStyle = "red";
                    this.ctx.fillRect(this.x * this.size, this.y * this.size, this.size, this.size);
                    break;
                case 1:
                    this.ctx.fillStyle = "black";
                    this.ctx.fillRect(this.x * this.size, this.y * this.size, this.size, this.size);
                    break;
            }
        }
    }]);

    return Food;
}(_Playground3.default);

Food.TYPE_FOOD = 0;
Food.TYPE_STONE = 1;

exports.default = Food;

/***/ },
/* 4 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Date = __webpack_require__(6);

var _Date2 = _interopRequireDefault(_Date);

var _Default = __webpack_require__(1);

var _Default2 = _interopRequireDefault(_Default);

var _Playground2 = __webpack_require__(0);

var _Playground3 = _interopRequireDefault(_Playground2);

var _Snake = __webpack_require__(5);

var _Snake2 = _interopRequireDefault(_Snake);

var _Food = __webpack_require__(3);

var _Food2 = _interopRequireDefault(_Food);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Main class of snake game
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
var Game = function (_Playground) {
    _inherits(Game, _Playground);

    /**
     * Constructor of snake game
     * Loads game. Needs to be started
     */
    function Game() {
        _classCallCheck(this, Game);

        var _this = _possibleConstructorReturn(this, (Game.__proto__ || Object.getPrototypeOf(Game)).call(this));

        _this.SPEED = 100;
        _this.NICKNAME = "Player 1";

        _this.SNAKE_SIZE = 10;
        _this.FOOD_SIZE = 10;
        _this.DIRECTION_UP = 0;
        _this.DIRECTION_DOWN = 1;
        _this.DIRECTION_LEFT = 2;
        _this.DIRECTION_RIGHT = 3;
        _this.started = false;

        _this.direction = _this.oldDirection = _this.DIRECTION_LEFT;
        _this.begin = _this.end = _this.gameloop = null;
        _this.moves = 0;

        _this.snake = new _Snake2.default(5, _this.SNAKE_SIZE);
        _this.foods = [new _Food2.default(_this.snake.elements, _Food2.default.TYPE_FOOD, _this.FOOD_SIZE)];
        return _this;
    }

    /**
     * Stops game
     */


    _createClass(Game, [{
        key: 'stop',
        value: function stop() {
            console.log('game finished');
            this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
            this.gameloop = cancelAnimationFrame(this.gameloop);
            this.started = false;
            this.end = new Date();
            console.log({
                name: this.NICKNAME,
                score: this.snake.score,
                moves: this.moves,
                duration: _Date2.default.getDifferenceInMilliseconds(this.begin, this.end),
                begin: _Date2.default.dateToTimestamp(this.begin),
                end: _Date2.default.dateToTimestamp(this.end)
            });
        }

        /**
         * Checks if food is in range of the head
         */

    }, {
        key: 'lookForFood',
        value: function lookForFood() {
            for (var i = 0; i < this.foods.length; i++) {
                if (Math.round(this.snake.elements[0].x - this.foods[i].x) === 0 && Math.round(this.snake.elements[0].y - this.foods[i].y) === 0) {

                    switch (this.foods[i].type) {
                        case _Food2.default.TYPE_FOOD:
                            this.snake.eat();
                            break;
                        case _Food2.default.TYPE_STONE:
                            this.stop();
                            break;
                        default:
                            this.snake.eat();
                            break;
                    }

                    if (this.snake.score > 30) for (var k = 0; k < this.foods.length; k++) {
                        this.foods[k] = new _Food2.default(this.snake.elements, this.foods[k].type, this.FOOD_SIZE);
                    } else this.foods[i] = new _Food2.default(this.snake.elements, this.foods[i].type, this.FOOD_SIZE);

                    if (this.snake.score % 5 === 0) this.foods = [].concat(_toConsumableArray(this.foods), [new _Food2.default(this.snake.elements, _Food2.default.TYPE_FOOD, this.FOOD_SIZE)]);

                    if (this.snake.score % 3 === 0) this.foods = [].concat(_toConsumableArray(this.foods), [new _Food2.default(this.snake.elements, _Food2.default.TYPE_STONE, this.FOOD_SIZE)]);

                    for (var _k = 0; _k < this.foods.length; _k++) {
                        for (var x = 0; x < this.foods.length; x++) {
                            if (this.foods[_k].x === this.foods[x].x && this.foods[_k].y === this.foods[x].y && x !== _k) this.foods[_k][_Default2.default.getRandomInt(1, 10) % 2 === 0 ? "x" : "y"]++;
                        }
                    }
                }
            }
        }

        /**
         * Returns function to listen on keys required for the game
         *
         * @param game
         * @returns {function()}
         * @private
         */

    }, {
        key: '_keyListener',
        value: function _keyListener(game) {
            return function (event) {
                var keyCode = event.keyCode ? event.keyCode : window.event.keyCode;

                switch (keyCode) {

                    case 37:
                        if (game.direction != game.DIRECTION_RIGHT && game.direction === game.oldDirection) {
                            game.direction = game.DIRECTION_LEFT;
                            game.moves++;
                        }
                        break;

                    case 39:
                        if (game.direction != game.DIRECTION_LEFT && game.direction === game.oldDirection) {
                            game.direction = game.DIRECTION_RIGHT;
                            game.moves++;
                        }
                        break;

                    case 38:
                        if (game.direction != game.DIRECTION_DOWN && game.direction === game.oldDirection) {
                            game.direction = game.DIRECTION_UP;
                            game.moves++;
                        }
                        break;

                    case 40:
                        if (game.direction != game.DIRECTION_UP && game.direction === game.oldDirection) {
                            game.direction = game.DIRECTION_DOWN;
                            game.moves++;
                        }
                        break;
                }
            };
        }

        /**
         * Main function of the game
         * Animates the snake
         *
         * @param game
         * @returns {function()}
         */

    }, {
        key: 'main',
        value: function main(game) {
            var _this2 = this;

            return function () {
                game.snake.elements[0].x = Math.round(game.snake.elements[0].x * 10) / 10;
                game.snake.elements[0].y = Math.round(game.snake.elements[0].y * 10) / 10;

                if (game.snake.elements[0].x % 1 === 0 && game.snake.elements[0].y % 1 === 0) game.oldDirection = game.direction;

                switch (game.oldDirection) {
                    case game.DIRECTION_RIGHT:
                        game.snake.elements[0].x += 0.01 * _this2.SPEED;
                        break;
                    case game.DIRECTION_LEFT:
                        game.snake.elements[0].x -= 0.01 * _this2.SPEED;
                        break;
                    case game.DIRECTION_UP:
                        game.snake.elements[0].y -= 0.01 * _this2.SPEED;
                        break;
                    case game.DIRECTION_DOWN:
                        game.snake.elements[0].y += 0.01 * _this2.SPEED;
                        break;
                }

                if (game.snake.check()) game.stop();

                game.lookForFood();
                game.snake.move();

                game.ctx.clearRect(0, 0, game.ctx.canvas.width, game.ctx.canvas.height);
                game.snake.draw();
                game.snake.setScoreText();

                var _iteratorNormalCompletion = true;
                var _didIteratorError = false;
                var _iteratorError = undefined;

                try {
                    for (var _iterator = game.foods[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                        var food = _step.value;

                        food.drawFood();
                    }
                } catch (err) {
                    _didIteratorError = true;
                    _iteratorError = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion && _iterator.return) {
                            _iterator.return();
                        }
                    } finally {
                        if (_didIteratorError) {
                            throw _iteratorError;
                        }
                    }
                }

                if (game.started) {
                    setTimeout(function () {
                        return game.gameloop = requestAnimationFrame(game.main(game));
                    }, 70);
                }
            };
        }

        /**
         * Function to start the game
         */

    }, {
        key: 'start',
        value: function start() {
            var name = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "Player 1";

            window.addEventListener("keydown", this._keyListener(this));
            this.started = true;
            this.begin = new Date();
            this.NICKNAME = name;
            this.gameloop = requestAnimationFrame(this.main(this));
        }
    }]);

    return Game;
}(_Playground3.default);

exports.default = Game;

/***/ },
/* 5 */
/***/ function(module, exports, __webpack_require__) {

"use strict";
'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _Default = __webpack_require__(1);

var _Default2 = _interopRequireDefault(_Default);

var _Playground2 = __webpack_require__(0);

var _Playground3 = _interopRequireDefault(_Playground2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Class of the game character (the snake)
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
var Snake = function (_Playground) {
    _inherits(Snake, _Playground);

    /**
     * Creates the snake at the center of the playground
     *
     * @param length
     * @param size
     */
    function Snake() {
        var length = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 4;
        var size = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 10;

        _classCallCheck(this, Snake);

        var _this = _possibleConstructorReturn(this, (Snake.__proto__ || Object.getPrototypeOf(Snake)).call(this));

        var X_START = Math.round(_this.w / 10 / 2);
        var Y_START = Math.round(_this.h / 10 / 2);

        _this.size = size;
        _this.elements = [];
        _this.score = 0;

        for (var i = X_START; i <= X_START + length; i++) {
            if (!_Default2.default.arrayContains(_this.elements, { x: i, y: Y_START })) _this.elements.push({ x: i, y: Y_START });
        }_this.setScoreText();
        return _this;
    }

    /**
     * Function to draw the body of the snake
     *
     * @param x
     * @param y
     * @private
     */


    _createClass(Snake, [{
        key: '_setBody',
        value: function _setBody(x, y) {
            this.ctx.fillStyle = "green";
            this.ctx.fillRect(x * this.size, y * this.size, this.size, this.size);
        }

        /**
         * Function to check if the snake bites itself
         *
         * @param x
         * @param y
         * @returns {boolean}
         * @private
         */

    }, {
        key: '_checkCollision',
        value: function _checkCollision(x, y) {
            for (var i = 1; i < this.elements.length; i++) {
                if (this.elements[i].x === x && this.elements[i].y === y) return true;
            }return false;
        }

        /**
         * Function to check if the snake collides with a border of the playground
         *
         * @param element
         * @returns {boolean}
         * @private
         */

    }, {
        key: '_checkBorder',
        value: function _checkBorder(element) {
            return element.x <= -1 || element.x >= Math.round(this.ctx.canvas.width / this.size) - 1 || element.y <= -1 || element.y >= Math.round(this.ctx.canvas.height / this.size) - 1;
        }

        /**
         * Draws the snake body on the canvas context
         */

    }, {
        key: 'draw',
        value: function draw() {
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = this.elements[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var element = _step.value;

                    this._setBody(element.x, element.y);
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }
        }

        /**
         * Adds a new element to the snake
         */

    }, {
        key: 'eat',
        value: function eat() {
            this.score++;
            this.setScoreText();
            this.elements = [].concat(_toConsumableArray(this.elements), [{
                x: this.elements[this.elements.length - 1].x,
                y: this.elements[this.elements.length - 1].y
            }]);
        }

        /**
         * Moves the snake
         */

    }, {
        key: 'move',
        value: function move() {
            var tail = this.elements.pop();
            if (this.elements.length > 0) {
                tail.x = this.elements[0].x;
                tail.y = this.elements[0].y;
            }

            this.elements.unshift(tail);
        }

        /**
         * Function to write the score on the canvas context
         */

    }, {
        key: 'setScoreText',
        value: function setScoreText() {
            var text = "Score: " + this.score;
            this.ctx.fillStyle = 'blue';
            this.ctx.font = "20px Arial";
            this.ctx.fillText(text, this.w / 2, this.h - 5);
        }

        /**
         * Checks if the game is lost
         *
         * @returns {boolean}
         */

    }, {
        key: 'check',
        value: function check() {
            return this._checkBorder(this.elements[0]) || this._checkCollision(this.elements[0].x, this.elements[0].y);
        }
    }]);

    return Snake;
}(_Playground3.default);

exports.default = Snake;

/***/ },
/* 6 */
/***/ function(module, exports) {

"use strict";
"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Helper class for events
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
var DateUtils = function () {
    function DateUtils() {
        _classCallCheck(this, DateUtils);
    }

    _createClass(DateUtils, null, [{
        key: "dateToTimestamp",

        /**
         * Converts a Date object to SQL timestamp
         *
         * @param date Date
         * @returns {string}
         */
        value: function dateToTimestamp(date) {

            var month = date.getMonth() + 1;
            if (month < 10) month = "0" + month;

            var day = date.getDate();
            if (day < 10) day = "0" + day;

            var hour = date.getHours();
            if (hour < 10) hour = "0" + hour;

            var minute = date.getMinutes();
            if (minute < 10) minute = "0" + minute;

            var second = date.getSeconds();
            if (second < 10) second = "0" + second;

            return date.getFullYear() + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
        }

        /**
         * Returns the date object in milliseconds
         *
         * @param dateString
         * @returns {number}
         */

    }, {
        key: "getMilliseconds",
        value: function getMilliseconds(dateString) {
            var date = dateString;

            if (!(dateString instanceof Date)) date = new Date(dateString);

            var totalMs = 0;
            totalMs += (date.getMonth() + 1) * 2630000000;
            totalMs += date.getDate() * 86400000;
            totalMs += date.getHours() * 3600000;
            totalMs += date.getMinutes() * 60000;
            totalMs += date.getSeconds() * 1000;

            return totalMs;
        }

        /**
         * Returns difference in milliseconds
         *
         * @param startdate
         * @param enddate
         * @returns {number}
         */

    }, {
        key: "getDifferenceInMilliseconds",
        value: function getDifferenceInMilliseconds(startdate) {
            var enddate = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

            return Math.abs(DateUtils.getMilliseconds(startdate) - DateUtils.getMilliseconds(enddate));
        }
    }]);

    return DateUtils;
}();

exports.default = DateUtils;

/***/ },
/* 7 */
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(2);


/***/ }
/******/ ]);
//# sourceMappingURL=build.js.map