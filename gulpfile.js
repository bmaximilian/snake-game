/**
 * Script to build the JavaScript application with gulp
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */
var gulp = require('gulp'),
    gulpUtil = require('gulp-util'),
    webpack = require('webpack'),
    webserver = require('gulp-webserver'),
    Server = require('karma').Server;
var webpackConfig = {
    context: __dirname,
    entry: {
        'build': [
            __dirname + '/src/index.js'
        ]
    },
    output: {
        filename: '[name].js',
        path: __dirname + '/dist/js',
        publicPath: '/js/'
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loader: require.resolve('babel-loader'),
                query: {
                    presets: [
                        require.resolve('babel-preset-es2015')
                    ]
                }
            }
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('production')
            }
        })
    ],
    node: {
        net: 'empty',
        dns: 'empty',
        fs: 'empty'
    }
};
gulp.task('test-javascript', ['webpack'], function (done) {
    new Server({
        configFile: __dirname + '/karma.conf.js',
        singleRun: true
    }, done).start();
    gulpUtil.log('[karma]', 'Javascript tested');
});

gulp.task('webpack', function (done) {
    var localConfig = Object.create(webpackConfig);
    localConfig.output.filename = '[name].js';
    localConfig.devtool = 'source-map';
    localConfig.debug = false;
    webpack(localConfig).run(function (err, stats) {
        if (err) throw new gulpUtil.PluginError('webpack:dev', err);
        gulpUtil.log('[webpack:dev]', stats.toString({
            colors: true
        }));
        done();
    });
});

gulp.task('serve', function () {
    gulp.src('dist')
        .pipe(webserver({
            livereload: true,
            open: true
        }));
});
gulp.task('build', [
    'webpack',
    'test-javascript',
]);
gulp.task('default', [
    'webpack',
]);