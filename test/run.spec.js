/**
 * First JavaScript test
 * Tests if Karma configuration works
 *
 * @author Maximilian Beck <contact@maximilianbeck.de>
 */

describe("testKarma", function () {

    it("works", function () {
        expect(1).toEqual(1);
    });

    it("compiles ES6", function () {

        const stringArray = ["H", "e", "l", "l", "o", " ", "W", "o", "r", "l", "d", "!"];
        let string = "";

        for (let character of stringArray) {
            string += character;
        }

        expect(string).toEqual("Hello World!");
    })

});
